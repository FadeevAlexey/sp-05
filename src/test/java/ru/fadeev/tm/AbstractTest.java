package ru.fadeev.tm;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.junit.experimental.categories.Category;
import org.springframework.util.Base64Utils;
import ru.fadeev.tm.client.ProjectClient;
import ru.fadeev.tm.client.TaskClient;
import ru.fadeev.tm.client.UserClient;

@Getter
@Setter
@Category(ru.fadeev.tm.IntegrationTest.class)
public abstract class AbstractTest {

    @NotNull final String serverUrl = "http://localhost:8080/";

    @NotNull final ProjectClient projectClient = ProjectClient.client(serverUrl);

    @NotNull final TaskClient taskClient = TaskClient.client(serverUrl);

    @NotNull final UserClient userClient = UserClient.client(serverUrl);

    @NotNull final String userProfile = auth("User","user");

    @NotNull final String adminProfile = auth("Admin","admin");

    protected String auth(@NotNull final String username, final String password){
        byte[] encodedBytes = Base64Utils.encode((username + ":" + password).getBytes());
        return "Basic " + new String(encodedBytes);
    }

}