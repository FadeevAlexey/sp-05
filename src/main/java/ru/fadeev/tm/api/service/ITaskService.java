package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.dto.TaskDTO;
import ru.fadeev.tm.entity.Task;

import java.util.Collection;

public interface ITaskService extends IService<Task> {

    @Nullable
    String findIdByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Collection<Task> findAll(@Nullable String userId);

    void removeAll(@Nullable String userId);

    @NotNull
    Collection<Task> sortByStartDate(@Nullable String userId);

    @NotNull
    Collection<Task> sortByFinishDate(@Nullable final String userId);

    @NotNull
    Collection<Task> sortByStatus(@Nullable final String userId);

    @NotNull
    Collection<Task> sortByCreationDate(@Nullable final String userId);

    @NotNull
    Collection<Task> searchByName(@Nullable String userId, @Nullable String string);

    @NotNull
    Collection<Task> searchByDescription(@Nullable String userId, @Nullable String string);

    @NotNull
    Collection<Task> findAllByProjectId(@Nullable String projectId, @Nullable String userId);

    void removeAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void removeAllProjectTask(@Nullable String userId);

    @Nullable
    Task findOne(@Nullable String userId, String id);

    void remove(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Task convertToTask(@NotNull final TaskDTO taskDTO);

}