package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.dto.ProjectDTO;
import ru.fadeev.tm.entity.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService extends IService<Project> {

    @Nullable
    String findIdByName(@Nullable String userId, @Nullable String name);

    @Nullable
    String findIdByName(@Nullable String name);

    @NotNull
    List<Project> findAll(@Nullable String userId);

    void removeAll(@Nullable String userId);

    @NotNull
    Collection<Project> sortByStartDate(@Nullable String userId);

    @NotNull
    Collection<Project> sortByFinishDate(@Nullable final String userId);

    @NotNull
    Collection<Project> sortByStatus(@Nullable final String userId);

    @NotNull
    Collection<Project> sortByCreationDate(@Nullable final String userId);

    @NotNull
    Collection<Project> searchByName(@Nullable String userId, @Nullable String string);

    @NotNull
    Collection<Project> searchByDescription(@Nullable String userId, @Nullable String string);

    @Nullable
    Project findOne(@Nullable String userId, @Nullable String id);

    void remove(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Project convertToProject(@NotNull final ProjectDTO projectDTO);

}