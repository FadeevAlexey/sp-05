package ru.fadeev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

@Getter
@Setter
public class SessionUser extends User {

    private String userId;

    public SessionUser(final User user) {
        super(
                user.getUsername(),
                user.getPassword(), user.isEnabled(),
                user.isAccountNonExpired(), user.isAccountNonExpired(),
                user.isCredentialsNonExpired(), user.getAuthorities()
        );

    }

    public SessionUser(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public SessionUser(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }

}
