package ru.fadeev.tm.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.fadeev.tm.enumerated.RoleType;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

@Getter
@Setter
@NoArgsConstructor
@Entity(name = "app_role")
public class Role extends AbstractEntity {

    @ManyToOne
    private User user;

    @Enumerated(value = EnumType.STRING)
    private RoleType role = RoleType.USER;

}