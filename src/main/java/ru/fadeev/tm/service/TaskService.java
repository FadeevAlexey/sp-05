package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.fadeev.tm.api.repository.IProjectRepository;
import ru.fadeev.tm.api.repository.ITaskRepository;
import ru.fadeev.tm.api.repository.IUserRepository;
import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.dto.TaskDTO;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.specification.Specifications;

import java.util.*;

@Service
@Transactional
public class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @Override
    public @NotNull List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void persist(@Nullable final Task task) {
        if (task == null) return;
        taskRepository.save(task);
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String userId, final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findOne(Specifications.findOne(userId, id)).orElse(null);
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        taskRepository.deleteById(id);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        taskRepository.removeByUser_IdAndId(userId, taskId);
    }

    @Override
    public void merge(@Nullable final Task task) {
        if (task == null) return;
        taskRepository.save(task);
    }

    @Override
    public void removeAll() {
        taskRepository.deleteAllInBatch();
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        taskRepository.removeAllByUser_Id(userId);
    }

    @NotNull
    @Override
    public Collection<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        return taskRepository.findAll(Specifications.findAllByUserId(userId));
    }

    @Override
    @NotNull
    public Collection<Task> findAllByProjectId(@Nullable final String projectId, @Nullable final String userId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAll(Specifications.findAllByProjectId(userId, projectId));
    }

    @Nullable
    @Override
    public String findIdByName(@Nullable final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        @Nullable final Task task =
                taskRepository.findOne(Specifications.findIdByName(userId, name)).orElse(null);
        return task == null ? null : task.getId();
    }

    @Override
    public void removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        taskRepository.removeAllByUser_IdAndProject_Id(userId, projectId);
    }

    @Override
    public void removeAllProjectTask(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        taskRepository.removeAllByUser_IdAndProject_IdNotNull(userId);
    }

    @NotNull
    @Override
    public Collection<Task> searchByName(@Nullable final String userId, @Nullable final String string) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (string == null || string.isEmpty()) return Collections.emptyList();
        return taskRepository.findAll(Specifications.searchByName(userId, string));
    }

    @NotNull
    @Override
    public Collection<Task> searchByDescription(@Nullable final String userId, @Nullable final String string) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (string == null || string.isEmpty()) return Collections.emptyList();
        return taskRepository.findAll(Specifications.searchByDescription(userId, string));
    }

    @NotNull
    @Override
    public Collection<Task> sortByStartDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAll(
                Specifications.findAllByUserId(userId),
                Sort.by(Sort.Direction.ASC, "startDate")
        );
    }

    @NotNull
    @Override
    public Collection<Task> sortByFinishDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAll(
                Specifications.findAllByUserId(userId),
                Sort.by(Sort.Direction.ASC, "finishDate")
        );
    }

    @NotNull
    @Override
    public Collection<Task> sortByStatus(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAll(
                Specifications.findAllByUserId(userId),
                Sort.by(Sort.Direction.ASC, "status")
        );
    }

    @NotNull
    @Override
    public Collection<Task> sortByCreationDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAll(
                Specifications.findAllByUserId(userId),
                Sort.by(Sort.Direction.ASC, "creationTime")
        );
    }

    @NotNull
    public Task convertToTask(@Nullable final TaskDTO taskDTO) {
        @Nullable Task task = findOne(taskDTO.getUserId(), taskDTO.getId());
        if (task == null) task = new Task();
        task.setId(taskDTO.getId());
        @Nullable User user = userRepository.findById(taskDTO.getUserId()).orElse(null);
        if (user != null) task.setUser(user);
        if (!taskDTO.getName().isEmpty()) task.setName(taskDTO.getName());
        if (taskDTO.getDescription() != null && !taskDTO.getDescription().isEmpty())
        task.setDescription(taskDTO.getDescription());
        final Project project = projectRepository.findOne(Specifications.findIdByName(taskDTO.getUserId(), taskDTO.getProjectName())).orElse(null);
        task.setProject(project);
        task.setStartDate(taskDTO.getStartDate());
        task.setFinishDate(taskDTO.getFinishDate());
        task.setStatus(taskDTO.getStatus());
        task.setCreationTime(taskDTO.getCreationTime());
        return task;
    }

}