package ru.fadeev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.dto.TaskDTO;
import ru.fadeev.tm.entity.SessionUser;
import ru.fadeev.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class TaskController {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @GetMapping(value = "/task")
    public ModelAndView taskListGet(@NotNull final Authentication authentication) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        @NotNull final List<TaskDTO> tasks = taskService.findAll(userId)
                .stream()
                .map(TaskDTO::new)
                .collect(Collectors.toList());
        @NotNull final ModelAndView model = new ModelAndView("task_list");
        model.addObject("login",sessionUser.getUsername());
        model.addObject("tasks", tasks);
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @GetMapping(value = "/task/create")
    public ModelAndView taskCreateGet(@NotNull final Authentication authentication) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        @NotNull final List<String> projects = new ArrayList<>();
        projectService.findAll(userId).forEach(project -> projects.add(project.getName()));
        @NotNull final ModelAndView model = new ModelAndView("task_create");
        model.addObject("login",sessionUser.getUsername());
        model.addObject("projects", projects);
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @PostMapping(value = "/task/create")
    public ModelAndView taskCreatePost(
            @NotNull final Authentication authentication,
            @NotNull final TaskDTO taskDTO
    ) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        taskDTO.setUserId(userId);
        @NotNull final Task task = taskService.convertToTask(taskDTO);
        taskService.persist(task);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/task");
        modelAndView.addObject("login",sessionUser.getUsername());
        modelAndView.setStatus(HttpStatus.MOVED_PERMANENTLY);
        return modelAndView;
    }

    @GetMapping(value = "/task/remove/{id}")
    public ModelAndView TaskRemoveGet(
            @NotNull final Authentication authentication,
            @PathVariable @NotNull final String id
    ) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        taskService.remove(userId, id);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/task");
        modelAndView.addObject("login",sessionUser.getUsername());
        modelAndView.setStatus(HttpStatus.MOVED_PERMANENTLY);
        return modelAndView;
    }

    @GetMapping(value = "/task/view/{id}")
    public ModelAndView taskViewGet(
            @NotNull final Authentication authentication,
            @PathVariable @NotNull final String id
    ) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        @Nullable final Task task = taskService.findOne(userId,id);
        @NotNull final ModelAndView model = new ModelAndView("task_view");
        model.addObject("login",sessionUser.getUsername());
        if (task == null) {
            model.setStatus(HttpStatus.BAD_REQUEST);
            return model;
        }
        model.addObject("task", new TaskDTO(task));
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @GetMapping(value = "/task/edit/{id}")
    public ModelAndView taskEditGet(
            @NotNull final Authentication authentication,
            @PathVariable @NotNull final String id
    ) {
        @NotNull final ModelAndView model = new ModelAndView("task_edit");
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        model.addObject("login",sessionUser.getUsername());
        @NotNull final String userId = sessionUser.getUserId();
        @NotNull final List<String> projects = new ArrayList<>();
        projectService.findAll(userId).forEach(project -> projects.add(project.getName()));
        @Nullable final Task task = taskService.findOne(id);
        if (task == null) {
            model.setStatus(HttpStatus.BAD_REQUEST);
            return model;
        }
        @Nullable final TaskDTO taskDTO = new TaskDTO(task);
        model.addObject("task", taskDTO);
        model.addObject("projects", projects);
        model.setStatus(HttpStatus.OK);
        return model;
    }

    @PostMapping(value = "/task/edit/{id}")
    public ModelAndView taskEditPost(
            @NotNull final Authentication authentication,
            @PathVariable @NotNull final String id,
            @NotNull final TaskDTO taskDTO
    ) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        taskDTO.setUserId(userId);
        taskDTO.setId(id);
        @Nullable final Task task = taskService.convertToTask(taskDTO);
        @NotNull ModelAndView model = new ModelAndView("redirect:/task");
        model.addObject("login",sessionUser.getUsername());
        taskService.merge(task);
        model.setStatus(HttpStatus.OK);
        return model;
    }

}