package ru.fadeev.tm.resources;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.dto.UserDTO;
import ru.fadeev.tm.entity.SessionUser;
import ru.fadeev.tm.entity.User;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class UserRestController {

    @NotNull
    @Autowired
    private IUserService userService;

    @GetMapping(value = "/api/users")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public ResponseEntity<List<UserDTO>> getUsers() {
        @NotNull final List<UserDTO> users = userService.findAll().stream()
                .map(UserDTO::new)
                .collect(Collectors.toList());
        return ResponseEntity.ok(users);
    }

    @PostMapping(value = "/api/user/create")
    public ResponseEntity<UserDTO> createUser(
            @RequestBody @NotNull final UserDTO userDTO
    ) {
        @NotNull final User user = userService.convertToUser(userDTO);
        userService.persist(user);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/api/user/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public ResponseEntity<UserDTO> deleteUserAdmin(
            @PathVariable @NotNull final String id
    ) {
        userService.remove(id);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/api/user")
    public ResponseEntity<UserDTO> deleteUser(
            @NotNull final Authentication authentication
    ) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        userService.remove(userId);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/api/user/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public ResponseEntity<UserDTO> userView(
            @PathVariable @NotNull final String id
    ) {
        @Nullable final User user = userService.findOne(id);
        if (user == null) return ResponseEntity.notFound().build();
        return ResponseEntity.ok(new UserDTO(user));
    }

    @PutMapping(value = "/api/user")
    public ResponseEntity<UserDTO> userEdit(
            @NotNull final Authentication authentication,
            @RequestBody @Nullable UserDTO userDTO
    ) {
        @NotNull final SessionUser sessionUser = (SessionUser) authentication.getPrincipal();
        @NotNull final String userId = sessionUser.getUserId();
        if (userDTO == null) return ResponseEntity.badRequest().build();
        userDTO.setId(userId);
        @NotNull final User user = userService.convertToUser(userDTO);
        userService.merge(user);
        return ResponseEntity.ok().build();
    }

}
